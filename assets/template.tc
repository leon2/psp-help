#include <stdlib.h>
#include <stdio.h>
#include <libgen.h>
#include <pthread.h>
#include <unistd.h> // for sleep
#include <sys/time.h> // for time... duh

#include "sim_avr.h"
#include "avr_ioport.h"
#include "sim_elf.h"
#include "sim_gdb.h"
#include "sim_vcd_file.h"

#include "helper/lcd.h"
#include "helper/inputs.h"

#include <string.h>

#define TEST_TIMEOUT #TIMEOUT

const char* fname = "#TESTNAME.elf";

avr_t * avr = NULL;

typedef struct button_t {
	avr_irq_t* irq; // output irq
	struct avr_t* avr;
	uint8_t value;
} button_t;

button_t button;

enum {
	IRQ_BUTTON_OUT = 0,
	IRQ_BUTTON_COUNT
};

void button_init(avr_t* avr, button_t* b, const char* name) {
	b->irq = avr_alloc_irq(&avr->irq_pool, 0, 1, &name);
	b->avr = avr;
}

void button_press(button_t* b) {
	printf("Pressed Button\n");
	avr_raise_irq(b->irq + IRQ_BUTTON_OUT, 0);
}
void button_release(button_t* b) {
	printf("Release Button\n");
	avr_raise_irq(b->irq + IRQ_BUTTON_OUT, 1);
}

void update() {
	update_lcd();
}

static void * avr_run_thread(void * oaram)
{
    #STATEVARS
    
    #STATEMACHINE

	while (1) {
		avr_run(avr);

		update();

        #RUNSM
	}

	return NULL;
}

int main() {
    elf_firmware_t f = {{0}};;
	
	//char path[256];

    //	sprintf(path, "%s/%s", dirname(argv[0]), fname);
    //	printf("Firmware pathname is %s\n", path);
	if(elf_read_firmware(fname, &f)) {
		printf("Could not read Firmware");
		exit(1);
	}

	avr = avr_make_mcu_by_name("atmega644");
	if (!avr) {
		exit(1);
	}
	avr_init(avr);
	avr_load_firmware(avr, &f);

	// initialize our 'peripheral'
	button_init(avr, &button, "button");
	// "connect" the output irw of the button to the port pin of the AVR
	avr_connect_irq(
		button.irq + IRQ_BUTTON_OUT,
		avr_io_getirq(avr, AVR_IOCTL_IOPORT_GETIRQ('A'), 3));

	init_lcd(avr);
	init_inputs(avr);

	printf("Starting \n\n");

	// the AVR run on it's own thread. it even allows for debugging!
	pthread_t run;
	pthread_create(&run, NULL, avr_run_thread, NULL);

    while (1) {
        sleep(TEST_TIMEOUT);

		printf("==== FAILED/TIMEOUT ====\n");
		printf("Timeout after %d seconds\n", TEST_TIMEOUT);
		printf("Display Content\n");
		print_all();

        exit(1);
    }
}
