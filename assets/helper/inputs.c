#include "inputs.h"
#include "sim_irq.h"
#include "avr_ioport.h"

typedef struct input_t {
    avr_irq_t* irq;
    struct avr_t* avr;
} input_t;

input_t esc;
input_t enter;
input_t btn2;
input_t btn3;

const char* esc_name = "ESC-Input";
const char* enter_name = "Enter-Input";
const char* btn2_name = "Button2-Input";
const char* btn3_name = "Button3-Input";

void init_inputs(struct avr_t* avr) {
    esc.irq = avr_alloc_irq(&avr->irq_pool, 0, 1, &esc_name);
    esc.avr = avr;
    avr_connect_irq(
		esc.irq + 0,
		avr_io_getirq(avr, AVR_IOCTL_IOPORT_GETIRQ('C'), 7));

    
    enter.irq = avr_alloc_irq(&avr->irq_pool, 0, 1, &enter_name);
    enter.avr = avr;
    avr_connect_irq(
		enter.irq + 0,
		avr_io_getirq(avr, AVR_IOCTL_IOPORT_GETIRQ('C'), 0));

    btn2.irq = avr_alloc_irq(&avr->irq_pool, 0, 1, &btn2_name);
    btn2.avr = avr;
    avr_connect_irq(
		btn2.irq + 0,
		avr_io_getirq(avr, AVR_IOCTL_IOPORT_GETIRQ('C'), 1));

    btn3.irq = avr_alloc_irq(&avr->irq_pool, 0, 1, &btn3_name);
    btn3.avr = avr;
    avr_connect_irq(
		btn3.irq + 0,
		avr_io_getirq(avr, AVR_IOCTL_IOPORT_GETIRQ('C'), 6));
}

void raw_press(input_t* input) {
    avr_raise_irq(input->irq + 0, 0); // press
}
void press_esc() {
    raw_press(&esc);
}
void press_enter() {
    raw_press(&enter);
}
void press_2() {
    raw_press(&btn2);
}
void press_3() {
    raw_press(&btn3);
}

void raw_release(input_t* input) {
    avr_raise_irq(input->irq + 0, 1); // release
}
void release_esc() {
    raw_release(&esc);
}
void release_enter() {
    raw_release(&enter);
}
void release_2() {
    raw_release(&btn2);
}
void release_3() {
    raw_release(&btn3);
}