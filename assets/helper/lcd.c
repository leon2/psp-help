#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "lcd.h"
#include "sim_avr.h"
#include "avr_ioport.h"

lcd_t lcd;

lcd_display_state_t* latest_display_state() {
	lcd_display_state_t* current = lcd.display_state;
	while (current->next != NULL) {
		current = current->next;
	}
	return current;	
}

void pin_changed_hook(struct avr_irq_t* irq, uint32_t value, void* param) {
	lcd.pin_state = (lcd.pin_state & ~(1 << irq->irq)) | (value << irq->irq);
	lcd.updated = 1;

	if ((lcd.pin_state & 0x0f) == 0x0f && lcd.state == LCD_STREAM_C) {
		// button_press(&button);
	}
}

void init_lcd(avr_t* avr) {
    lcd.pin_state = 0;
    lcd.state = LCD_UNINIT;
    lcd.updated = 0;
    lcd.init_count = 0;
    lcd.stream_byte1 = 0;
    lcd.stream_byte2 = 0;

	lcd.display_state = malloc(sizeof(lcd_display_state_t));
	lcd.display_state->content = calloc(100, 1);
	lcd.display_state->size = 100;
	lcd.display_state->index = 0;
	lcd.display_state->next = NULL;

	lcd.display_last = lcd.display_state;

    for (int i = 0; i < 8; i++)
		avr_irq_register_notify(
			avr_io_getirq(avr, AVR_IOCTL_IOPORT_GETIRQ('A'), i),
			pin_changed_hook, 
			NULL);
}

void update_lcd() {
    if (!lcd.updated) {
		// return;
	}

	if (lcd.state == LCD_UNINIT) {
		if (lcd.pin_state == 0x3) {
			printf("Starting Init \n");
			lcd.state = LCD_INITS;
		}
	} else if (lcd.state == LCD_INITS) {
		if (lcd.pin_state & 0b00100000) {
			lcd.state = LCD_INITC;
		}
	} else if (lcd.state == LCD_INITC) {
		if ((lcd.pin_state & 0b00100000) == 0) {
			lcd.state = LCD_INITS;
			lcd.init_count++;

			if (lcd.init_count >= 3) {
				lcd.state = LCD_BITMODE;
			}
		}
	} else if (lcd.state == LCD_BITMODE) {
		if (lcd.pin_state == 0x02) {
			printf("4Bit Mode \n");

			lcd.state = LCD_BITMODES;
		} else {
			// printf("[BITMODE] %d \n", pin_state);
		}
	} else if (lcd.state == LCD_BITMODES) {
		if (lcd.pin_state & 0b00100000) {
			lcd.state = LCD_BITMODEC;
		}
	} else if (lcd.state == LCD_BITMODEC) {
		if ((lcd.pin_state & 0b00100000) == 0) {
			lcd.state = LCD_RUNNING;
		}
	} else if (lcd.state == LCD_RUNNING) {
		// printf("State %d \n", pin_state);

		if (lcd.pin_state == 0x40) {
			// printf("RW set -> starting sendStream \n");
			lcd.state = LCD_STREAM_S;
			lcd.init_count = 0;
		}
	} else if (lcd.state == LCD_STREAM_S) {
		if (lcd.pin_state & 0b00100000) {
			lcd.state = LCD_STREAM_C;
		}
	} else if (lcd.state == LCD_STREAM_C) {
		if ((lcd.pin_state & 0b00100000) == 0) {
			lcd.state = LCD_STREAM_S;
			lcd.init_count++;

			if (lcd.init_count >= 2) {
				lcd.state = LCD_STREAM_B1;
			}
		}
	} else if (lcd.state == LCD_STREAM_B1) {
		// printf("First Byte %x \n", lcd.pin_state);

		lcd.stream_byte1 = lcd.pin_state;
		lcd.state = LCD_STREAM_B1S;
	} else if (lcd.state == LCD_STREAM_B1S) {
		if (lcd.pin_state & 0b00100000) {
			lcd.state = LCD_STREAM_B1C;
		} else {
			lcd.stream_byte1 = lcd.pin_state;
		}
	} else if (lcd.state == LCD_STREAM_B1C) {
		if ((lcd.pin_state & 0b00100000) == 0) {
			lcd.state = LCD_STREAM_B2;
		}
	} else if (lcd.state == LCD_STREAM_B2) {
		// printf("Second Byte %x \n", lcd.pin_state);

		lcd.stream_byte2 = lcd.pin_state;
		lcd.state = LCD_STREAM_B2S;
	} else if (lcd.state == LCD_STREAM_B2S) {
		if (lcd.pin_state & 0b00100000) {
			lcd.state = LCD_STREAM_B2C;
		}
	} else if (lcd.state == LCD_STREAM_B2C) {
		if ((lcd.pin_state & 0b00100000) == 0) {
			lcd.state = LCD_RUNNING;

			uint8_t combined = ((lcd.stream_byte1 & 0xf) << 4) | (lcd.stream_byte2 & 0xf);

			if ((lcd.stream_byte1 & 0xf0) == 0x10 && (lcd.stream_byte2 & 0xf0) == 0x10 && combined != '\0' && combined != '\n') {
				// Write a single character

				lcd_display_state_t* latest = latest_display_state();
				if (latest->index >= latest->size-2) {
					int nsize = 2 * latest->size;
					char* nbuffer = calloc(nsize, 1);

					for (int i = 0; i < latest->size; i++) {
						nbuffer[i] = latest->content[i];
					}

					free(latest->content);

					latest->size = nsize;
					latest->content = nbuffer;
				}

				latest->content[latest->index] = (char) combined;
				latest->index++;
			} else if (combined & 0x01) {
				// Clean the Display
				
				lcd_display_state_t* n_state = malloc(sizeof(lcd_display_state_t));
				n_state->content = calloc(100, 1);
				n_state->size = 100;
				n_state->index = 0;
				n_state->next = NULL;

				lcd.display_last->next = n_state;
				lcd.display_last = n_state;
			} else if (combined == 0x80) {
				lcd_display_state_t* n_state = malloc(sizeof(lcd_display_state_t));
				n_state->content = calloc(100, 1);
				n_state->size = 100;
				n_state->index = 0;
				n_state->next = NULL;

				lcd.display_last->next = n_state;
				lcd.display_last = n_state;
			} else if (combined == 0xc0) {
				// printf("Line 2 \n");
			} else if (combined == 0x00) {
				// printf("Move Cursor Left\n");
				// lcd.write_index--;
			} else {
				/*
                printf("Raw Bytes %x %x \n", lcd.stream_byte1, lcd.stream_byte2);
			    printf("Both Bytes %x %x\n", (lcd.stream_byte1 & 0xf)<<4, lcd.stream_byte2 & 0xf);
                printf("Combined %x \n", combined);
				*/
            }
		}
	}

	lcd.updated = 0;
}

char* current_content() {
	return lcd.display_last->content;
}

void print_all() {
	lcd_display_state_t* current = lcd.display_state;
	while (current != NULL) {
		if (current->index != 0) {
			printf("Content: '%s'\n", current->content);
		}

		current = current->next;
	}	
}