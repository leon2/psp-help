#ifndef _INPUTS_H
#define _INPUTS_H

#include "sim_avr.h"

void init_inputs(struct avr_t* avr);

void press_esc();
void press_enter();
void press_2();
void press_3();

void release_esc();
void release_enter();
void release_2();
void release_3();

#endif