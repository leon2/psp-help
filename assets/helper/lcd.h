#include "sim_avr.h"

#ifndef _LCD_H_
#define _LCD_H_

typedef enum lcd_state_t {
	LCD_UNINIT,
	// Expect SBI
	LCD_INITS,
	// Expect CBI
	LCD_INITC,
	// Expect BitMode
	LCD_BITMODE,
	// Expect SBI
	LCD_BITMODES,
	// Expect CBI
	LCD_BITMODEC,
	LCD_RUNNING,
	LCD_STREAM_S,
	LCD_STREAM_C,
	LCD_STREAM_B1,
	LCD_STREAM_B1S,
	LCD_STREAM_B1C,	
	LCD_STREAM_B2,
	LCD_STREAM_B2S,
	LCD_STREAM_B2C,
} lcd_state_t;

typedef struct lcd_display_state_t {
	char* content;
	int size;
	int index;
	struct lcd_display_state_t* next;
} lcd_display_state_t;

typedef struct lcd_t {
    uint8_t pin_state;
    lcd_state_t state;
    uint8_t updated;
    uint8_t init_count;
    uint8_t stream_byte1;
    uint8_t stream_byte2;
	lcd_display_state_t* display_state;
	lcd_display_state_t* display_last;
} lcd_t;

extern lcd_t lcd;

// Initializes the LCD state and general setup
void init_lcd(avr_t* avr);

// Update the LCD State
void update_lcd();

// Get the current LCD Display content
char* current_content();

// Prints all the Display States that were tracked
void print_all();

#endif