# PSP-Help
A collection of tools to make PSP easier to test and manage

## Example
`cargo run -- gen-test --test-name Free --input ./examples/single/basic.pst --output test.c`

## Statemachine-Language
A DSL (Domain Specific Language) specifically designed to specify/design a Test Case.

At the Top-Level every specification consists of different State definitions. The Statemachine
will always start in a `start` State, which needs to be defined, an end State does not need to be
specified explicitly.

### State Definition
```
[name] {
    [statements]
}
```
The Name can be any string, that only contains letters and no spaces.
The statements can be any sequence of statements.

### Statement
`[statement];`

#### Statements
* `press([button])`
* `release([button])`
* `passed()`
* `failed()`
* `printDisplay()`
* `transition([target state])`
* `transitionWithPauseMs([target state], [duration in ms])`
* `if([condition]) { [statements] }`

### Condition
* `containsText("[text]")`

### Buttons
* `enter`
* `esc`
* `up`
* `down`