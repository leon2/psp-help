FROM rust:1.65.0 as builder

WORKDIR /usr/src/psp-helper
COPY . .

RUN cargo install --path .

FROM ubuntu:22.04

RUN apt-get update
RUN apt-get install -y sed git make binutils gcc-avr avr-libc gcc elfutils libelf-dev pkg-config
COPY --from=builder /usr/local/cargo/bin/psp-helper /usr/bin/psp-helper