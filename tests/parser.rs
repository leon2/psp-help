use psp_helper::{ParseError, ProcessError, StaticSource, Token};

#[test]
fn if_missing_paren() {
    let content = "first {
        if containsText(\"\") {}
    }";

    let result = psp_helper::process(StaticSource::new("test-input", content));
    let result_err = result.unwrap_err();
    /*
    assert_eq!(
        ProcessError::Parse(ParseError::UnexpectedToken {
            expected: Token::ParenOpen,
            received: Token::Raw("containsText".to_string())
        }),
        result_err
    );
    */
}
