use psp_helper::{ParseError, ProcessError, StaticSource};

#[test]
fn all() {
    let content = "start {
        press(enter);
        press(esc);
        press(up);
        press(down);

        release(enter);
        release(esc);
        release(up);
        release(down);

        printDisplay();

        passed();
        failed();

        transition(other);
        transitionWithPauseMs(other, 50);
    } other {}";

    psp_helper::process(StaticSource::new("test-input", content))
        .expect("Should not return an error");
}

#[test]
fn unknown_func() {
    let content = "start {
        unknown_function();
    }";

    let result = psp_helper::process(StaticSource::new("test-input", content));
    let result_err = result.unwrap_err();
    /*
    assert_eq!(
        ProcessError::Parse(ParseError::UnknownFunction("unknown_function".to_string())),
        result_err
    );
    */
}
