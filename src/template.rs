mod cpu;
pub use cpu::CpuTemplate;

mod test;
pub use test::{TestTemplate, TestTemplateBuilder};

mod makefile;
pub use makefile::MakefileTemplate;
