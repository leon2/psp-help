use std::sync::Arc;

use crate::span::{Source, Span};

#[derive(Debug, PartialEq, Clone)]
pub enum Token {
    Raw(String),
    StringLiteral(String),
    ParenOpen,
    ParenClose,
    CurlyOpen,
    CurlyClose,
    Semicolon,
    Comma,
}

impl Token {
    pub fn display_str(&self) -> &'static str {
        match self {
            Self::Raw(_) => "Identifier",
            Self::StringLiteral(_) => "String Literal",
            Self::ParenOpen => "(",
            Self::ParenClose => ")",
            Self::CurlyOpen => "{",
            Self::CurlyClose => "}",
            Self::Semicolon => ";",
            Self::Comma => ",",
        }
    }
}

#[derive(Debug, Clone, Copy)]
enum TokenizerState {
    Normal,
    StringLiteral,
}

pub struct Tokenizer<S> {
    source: Arc<S>,
    index: usize,
    last: Option<Span<Arc<S>, Token>>,
    content_start: usize,
    content: String,
    state: TokenizerState,
}

impl<S> Iterator for Tokenizer<S>
where
    S: Source,
{
    type Item = Span<Arc<S>, Token>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(last) = self.last.take() {
            return Some(last);
        }

        let span_content = self.source.content();
        let iter = core::iter::from_fn(|| {
            let mut tmp_iter = span_content.char_indices().skip(self.index);

            let res = tmp_iter.next()?;
            self.index += res.1.len_utf8();
            Some((res.1, res.0))
        });

        for (next, index) in iter {
            match self.state {
                TokenizerState::Normal => {
                    match next {
                        '(' if !self.content.is_empty() => {
                            self.last = Some(
                                self.source
                                    .clone()
                                    .sub_span(index..index + 1, Token::ParenOpen),
                            );

                            let raw = core::mem::take(&mut self.content);
                            return Some(self.source.clone().sub_span(
                                self.content_start..(self.content_start + raw.as_bytes().len()),
                                Token::Raw(raw),
                            ));
                        }
                        '(' => {
                            self.content_start = index + 1;
                            return Some(self.source.clone().sub_span(
                                self.content_start - 1..self.content_start,
                                Token::ParenOpen,
                            ));
                        }
                        ')' if !self.content.is_empty() => {
                            self.last = Some(
                                self.source
                                    .clone()
                                    .sub_span(index..index + 1, Token::ParenClose),
                            );

                            let raw = core::mem::take(&mut self.content);
                            return Some(self.source.clone().sub_span(
                                self.content_start..(self.content_start + raw.as_bytes().len()),
                                Token::Raw(raw),
                            ));
                        }
                        ')' => {
                            self.content_start = index + 1;
                            return Some(self.source.clone().sub_span(
                                self.content_start - 1..self.content_start,
                                Token::ParenClose,
                            ));
                        }
                        '{' if !self.content.is_empty() => {
                            self.last = Some(
                                self.source
                                    .clone()
                                    .sub_span(index..index + 1, Token::CurlyOpen),
                            );

                            let raw = core::mem::take(&mut self.content);
                            return Some(self.source.clone().sub_span(
                                self.content_start..(self.content_start + raw.as_bytes().len()),
                                Token::Raw(raw),
                            ));
                        }
                        '{' => {
                            self.content_start = index + 1;
                            return Some(self.source.clone().sub_span(
                                self.content_start - 1..self.content_start,
                                Token::CurlyOpen,
                            ));
                        }
                        '}' if !self.content.is_empty() => {
                            self.last = Some(
                                self.source
                                    .clone()
                                    .sub_span(index..index + 1, Token::CurlyClose),
                            );

                            let raw = core::mem::take(&mut self.content);
                            return Some(self.source.clone().sub_span(
                                self.content_start..(self.content_start + raw.as_bytes().len()),
                                Token::Raw(raw),
                            ));
                        }
                        '}' => {
                            self.content_start = index + 1;
                            return Some(self.source.clone().sub_span(
                                self.content_start - 1..self.content_start,
                                Token::CurlyClose,
                            ));
                        }
                        ';' if !self.content.is_empty() => {
                            self.last = Some(
                                self.source
                                    .clone()
                                    .sub_span(index..index + 1, Token::Semicolon),
                            );

                            let raw = core::mem::take(&mut self.content);
                            return Some(self.source.clone().sub_span(
                                self.content_start..(self.content_start + raw.as_bytes().len()),
                                Token::Raw(raw),
                            ));
                        }
                        ';' => {
                            self.content_start = index + 1;
                            return Some(self.source.clone().sub_span(
                                self.content_start - 1..self.content_start,
                                Token::Semicolon,
                            ));
                        }
                        ',' if !self.content.is_empty() => {
                            self.last =
                                Some(self.source.clone().sub_span(index..index + 1, Token::Comma));

                            let raw = core::mem::take(&mut self.content);
                            return Some(self.source.clone().sub_span(
                                self.content_start..(self.content_start + raw.as_bytes().len()),
                                Token::Raw(raw),
                            ));
                        }
                        ',' => {
                            self.content_start = index + 1;
                            return Some(self.source.clone().sub_span(
                                self.content_start - 1..self.content_start,
                                Token::Comma,
                            ));
                        }
                        '\n' | ' ' if !self.content.is_empty() => {
                            let raw = core::mem::take(&mut self.content);
                            let res = Some(self.source.clone().sub_span(
                                self.content_start..(self.content_start + raw.as_bytes().len()),
                                Token::Raw(raw),
                            ));

                            self.content_start = index + 1;

                            return res;
                        }
                        '\n' | ' ' => {
                            self.content_start = index + 1;
                            continue;
                        }
                        '"' if !self.content.is_empty() => {
                            self.state = TokenizerState::StringLiteral;
                            let raw = core::mem::take(&mut self.content);
                            return Some(self.source.clone().sub_span(
                                self.content_start..(self.content_start + raw.as_bytes().len()),
                                Token::Raw(raw),
                            ));
                        }
                        '"' => {
                            self.state = TokenizerState::StringLiteral;
                            self.content_start = index + 1;
                            continue;
                        }
                        other => {
                            self.content.push(other);
                        }
                    };
                }
                TokenizerState::StringLiteral => {
                    match next {
                        '"' => {
                            self.state = TokenizerState::Normal;
                            let raw = core::mem::take(&mut self.content);
                            return Some(self.source.clone().sub_span(
                                self.content_start..(self.content_start + raw.as_bytes().len()),
                                Token::StringLiteral(raw),
                            ));
                        }
                        other => {
                            self.content.push(other);
                        }
                    };
                }
            };
        }

        None
    }
}

pub fn tokenizer<S>(source: S) -> impl Iterator<Item = Span<Arc<S>, Token>>
where
    S: Source,
{
    Tokenizer {
        source: Arc::new(source),
        index: 0,
        last: None,
        content_start: 0,
        content: String::new(),
        state: TokenizerState::Normal,
    }
}
