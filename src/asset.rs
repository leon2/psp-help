use std::path::Path;

pub trait Asset {
    fn write(&self, base: &Path) -> Result<(), std::io::Error>;
}

pub struct AssetFile {
    name: &'static str,
    content: &'static str,
}

impl AssetFile {
    pub const fn new(name: &'static str, content: &'static str) -> Self {
        Self { name, content }
    }
}

impl Asset for AssetFile {
    fn write(&self, base: &Path) -> Result<(), std::io::Error> {
        let target = base.join(self.name);
        std::fs::write(target, self.content)
    }
}

pub struct AssetFolder {
    name: &'static str,
    assets: Vec<Box<dyn Asset>>,
}

impl AssetFolder {
    pub fn new<IT>(name: &'static str, parts: IT) -> Self
    where
        IT: IntoIterator<Item = Box<dyn Asset>>,
    {
        Self {
            name,
            assets: parts.into_iter().collect(),
        }
    }
}

impl Asset for AssetFolder {
    fn write(&self, base: &Path) -> Result<(), std::io::Error> {
        let folder = base.join(self.name);
        if !folder.exists() {
            std::fs::create_dir_all(&folder)?;
        }

        for entry in self.assets.iter() {
            entry.write(&folder)?;
        }

        Ok(())
    }
}
