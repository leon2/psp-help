use std::{collections::HashMap, sync::Arc, time::Duration};

mod span;
use span::Span;
pub use span::{FileSource, SingleSourceCache, Source, StaticSource};

mod tokenizer;
pub use tokenizer::Token;

mod scoped;
use scoped::scoped_iter;

mod template;
pub use template::{CpuTemplate, MakefileTemplate, TestTemplate};

mod button;
use button::ButtonPressed;

pub mod asset;

pub struct OnDrop {
    func: Box<dyn FnMut()>,
}
impl OnDrop {
    pub fn new<F>(func: F) -> Self
    where
        F: FnMut() + 'static,
    {
        Self {
            func: Box::new(func),
        }
    }
}
impl Drop for OnDrop {
    fn drop(&mut self) {
        (self.func)();
    }
}

#[derive(Debug)]
pub struct StateMachine {
    states: Vec<State>,
}

#[derive(Debug, Clone)]
enum Statement {
    Conditional {
        condition: Condition,
        body: Vec<Statement>,
    },
    FunctionCall(Function),
}

#[derive(Debug, Clone)]
enum Condition {
    ContainsText(String),
}

#[derive(Debug, Clone)]
enum Function {
    Press(ButtonPressed),
    Release(ButtonPressed),
    Passed,
    Failed,
    Transition(String),
    TransitionWithPause(String, Duration),
    PrintEntireDisplay,
}

#[derive(Debug)]
struct State {
    name: String,
    body: Vec<Statement>,
}

fn parse_condition<S>(
    mut inner: impl Iterator<Item = Span<S, Token>>,
) -> Result<Condition, ParseError<S>>
where
    S: span::Source,
{
    let raw_first = match inner.next() {
        Some(elem) => elem,
        None => return Err(ParseError::UnexpectedEOF),
    };
    let first = match raw_first.content() {
        Token::Raw(r) => r.to_string(),
        _ => {
            return Err(ParseError::UnexpectedToken {
                expected: Token::Raw("".to_string()),
                received: raw_first,
            })
        }
    };

    match first.as_str() {
        "containsText" => {
            check_token(inner.next(), Token::ParenOpen)?;

            let target_text = match inner.next() {
                Some(elem) => match elem.content() {
                    Token::StringLiteral(s) => s.to_string(),
                    _ => {
                        return Err(ParseError::UnexpectedToken {
                            expected: Token::StringLiteral("".to_string()),
                            received: elem,
                        })
                    }
                },
                None => return Err(ParseError::UnexpectedEOF),
            };

            check_token(inner.next(), Token::ParenClose)?;
            let _ = inner.next();

            Ok(Condition::ContainsText(target_text))
        }
        unknown => {
            let unknown = unknown.to_string();
            Err(ParseError::UnknownCondition(raw_first.map(|_| unknown)))
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum ParseError<S> {
    UnexpectedEOF,
    UnexpectedToken {
        expected: Token,
        received: Span<S, Token>,
    },
    UnknownCondition(Span<S, String>),
    UnknownFunction(Span<S, String>),
}

fn check_token<S>(opt: Option<Span<S, Token>>, expected: Token) -> Result<(), ParseError<S>>
where
    S: span::Source,
{
    let tok = match opt {
        Some(t) => t,
        None => return Err(ParseError::UnexpectedEOF),
    };

    if tok.content() != &expected {
        return Err(ParseError::UnexpectedToken {
            expected,
            received: tok,
        });
    }

    Ok(())
}

fn parse_statement<S>(
    mut inner: impl Iterator<Item = Span<S, Token>>,
) -> Result<Statement, ParseError<S>>
where
    S: span::Source,
{
    let raw_key = inner.next().ok_or(ParseError::UnexpectedEOF)?;
    let key = match raw_key.content() {
        Token::Raw(r) => r,
        _ => {
            return Err(ParseError::UnexpectedToken {
                expected: Token::Raw("".to_string()),
                received: raw_key,
            })
        }
    };
    let key = key.trim();
    if key.is_empty() {
        return Err(ParseError::UnexpectedEOF);
    }

    match key {
        "if" => {
            check_token(inner.next(), Token::ParenOpen)?;

            let condition_iter = scoped_iter(inner.by_ref(), Token::ParenOpen, Token::ParenClose);
            let condition = parse_condition(condition_iter)?;

            check_token(inner.next(), Token::CurlyOpen)?;

            let mut condition_body_iter =
                scoped_iter(inner.by_ref(), Token::CurlyOpen, Token::CurlyClose);
            let condition_body = parse_statements(
                &mut condition_body_iter as &mut dyn Iterator<Item = Span<S, Token>>,
            )?;

            Ok(Statement::Conditional {
                condition,
                body: condition_body,
            })
        }
        name => {
            check_token(inner.next(), Token::ParenOpen)?;

            let arguments_iter = scoped_iter(inner.by_ref(), Token::ParenOpen, Token::ParenClose);
            let mut arguments_raw: Vec<_> = arguments_iter.collect();

            let func = match name {
                "press" => {
                    assert!(arguments_raw.len() == 1);
                    let raw_first = arguments_raw.remove(0);
                    let first = match raw_first.content() {
                        Token::Raw(content) => ButtonPressed::parse(content).unwrap(),
                        other => panic!("{:?}", other),
                    };
                    Function::Press(first)
                }
                "release" => {
                    assert!(arguments_raw.len() == 1);
                    let raw_first = arguments_raw.remove(0);
                    let first = match raw_first.content() {
                        Token::Raw(content) => ButtonPressed::parse(content).unwrap(),
                        other => panic!("{:?}", other),
                    };
                    Function::Release(first)
                }
                "passed" => Function::Passed,
                "failed" => Function::Failed,
                "printDisplay" => Function::PrintEntireDisplay,
                "transition" => {
                    assert!(arguments_raw.len() == 1);
                    let raw_first = arguments_raw.remove(0);
                    let first = match raw_first.content() {
                        Token::Raw(f) => f,
                        _ => {
                            return Err(ParseError::UnexpectedToken {
                                expected: Token::Raw("".to_string()),
                                received: raw_first,
                            })
                        }
                    };
                    Function::Transition(first.to_string())
                }
                "transitionWithPauseMs" => {
                    assert!(arguments_raw.len() == 3);

                    let raw_first = arguments_raw.remove(0);
                    let first = match raw_first.content() {
                        Token::Raw(f) => f,
                        _ => {
                            return Err(ParseError::UnexpectedToken {
                                expected: Token::Raw("".to_string()),
                                received: raw_first,
                            })
                        }
                    };

                    check_token(Some(arguments_raw.remove(0)), Token::Comma)?;

                    let raw_duration = arguments_raw.remove(0);
                    let duration: u64 = match raw_duration.content() {
                        Token::Raw(f) => f.parse().unwrap(),
                        _ => {
                            return Err(ParseError::UnexpectedToken {
                                expected: Token::Raw("".to_string()),
                                received: raw_duration,
                            })
                        }
                    };

                    Function::TransitionWithPause(
                        first.to_string(),
                        Duration::from_millis(duration),
                    )
                }
                other => {
                    let fname = other.to_string();
                    return Err(ParseError::UnknownFunction(raw_key.map(|_| fname)));
                }
            };

            check_token(inner.next(), Token::Semicolon)?;

            Ok(Statement::FunctionCall(func))
        }
    }
}

fn parse_statements<S>(
    inner: impl Iterator<Item = Span<S, Token>>,
) -> Result<Vec<Statement>, ParseError<S>>
where
    S: Source,
{
    let mut result = Vec::new();

    let mut inner = inner.peekable();

    loop {
        if inner.peek().is_none() {
            break;
        }

        let stmnt = parse_statement(&mut inner)?;
        result.push(stmnt);
    }

    Ok(result)
}

fn parse_state<S>(
    name: String,
    inner: impl Iterator<Item = Span<S, Token>>,
) -> Result<State, ParseError<S>>
where
    S: Source,
{
    let content = parse_statements(inner)?;

    Ok(State {
        name,
        body: content,
    })
}

fn parse<S>(content: S) -> Result<StateMachine, ParseError<Arc<S>>>
where
    S: Source,
{
    let mut content_iter = tokenizer::tokenizer(content);

    let mut states = Vec::new();
    loop {
        let name = match content_iter.next() {
            Some(t) => match t.content() {
                Token::Raw(r) if !r.chars().all(|c| c.is_alphabetic()) => {
                    todo!("Non alphabetical Name")
                }
                Token::Raw(r) if r.is_empty() => {
                    todo!("Empty Name")
                }
                Token::Raw(_) => t.map(|r| match r {
                    Token::Raw(n) => n,
                    other => unreachable!(
                        "Previously checked that its a Raw String, but got {:?}",
                        other
                    ),
                }),
                _ => {
                    return Err(ParseError::UnexpectedToken {
                        expected: Token::Raw("".to_string()),
                        received: t,
                    })
                }
            },
            None => break,
        };

        assert_eq!(
            Some(Token::CurlyOpen),
            content_iter.next().map(|c| c.content().clone())
        );

        let inner_iter = scoped_iter(content_iter.by_ref(), Token::CurlyOpen, Token::CurlyClose);
        let pstate = parse_state(name.content().to_string(), inner_iter)?;

        states.push(pstate);
    }

    Ok(StateMachine { states })
}

fn validate_condition(condition: &Condition, _states: &[State]) {
    match condition {
        Condition::ContainsText(_) => {}
    };
}

fn validate_statement(statement: &Statement, states: &[State]) {
    match statement {
        Statement::Conditional { condition, body } => {
            validate_condition(condition, states);
            for stmnt in body.iter() {
                validate_statement(stmnt, states);
            }
        }
        Statement::FunctionCall(func) => match func {
            Function::Passed => {}
            Function::Failed => {}
            Function::PrintEntireDisplay => {}
            Function::Press(_) => {}
            Function::Release(_) => {}
            Function::Transition(target) | Function::TransitionWithPause(target, _) => {
                if !states.iter().any(|s| &s.name == target) {
                    panic!("")
                }
            }
        },
    };
}

fn validate_sm(sm: &StateMachine) {
    for state in sm.states.iter() {
        for statement in state.body.iter() {
            validate_statement(statement, &sm.states);
        }
    }

    if !sm.states.iter().any(|s| s.name.as_str() == "start") {
        panic!("Missing 'start' State");
    }
}

#[derive(Debug, PartialEq)]
pub enum ProcessError<S> {
    Parse(ParseError<Arc<S>>),
}

pub fn process<S>(content: S) -> Result<StateMachine, ProcessError<S>>
where
    S: span::Source,
{
    let parsed = parse(content).map_err(ProcessError::Parse)?;

    validate_sm(&parsed);

    Ok(parsed)
}

#[derive(Debug, PartialEq, Eq, Hash)]
enum StateName {
    Name(String),
    NameWaiting(String),
}

impl Statement {
    fn generate(&self, state_ids: &HashMap<StateName, usize>) -> String {
        match self {
            Self::Conditional { condition, body } => {
                let cond_str = match condition {
                    Condition::ContainsText(text) => {
                        format!("strstr(current_content(), \"{}\")", text)
                    }
                };

                let body_str: String = body.iter().map(|stmnt| stmnt.generate(state_ids)).collect();

                format!("if({}) {{ {} }}", cond_str, body_str)
            }
            Self::FunctionCall(func) => match func {
                Function::Passed => "printf(\"\\n==== PASSED ====\\n\");exit(0);".to_string(),
                Function::Failed => "printf(\"\\n==== FAILED ====\\n\");exit(1);".to_string(),
                Function::PrintEntireDisplay => "print_all();".to_string(),
                Function::Press(btn) => match btn {
                    ButtonPressed::Enter => "press_enter();".to_string(),
                    ButtonPressed::Esc => "press_esc();".to_string(),
                    ButtonPressed::Up => "press_2();".to_string(),
                    ButtonPressed::Down => "press_3();".to_string(),
                },
                Function::Release(btn) => match btn {
                    ButtonPressed::Enter => "release_enter();".to_string(),
                    ButtonPressed::Esc => "release_esc();".to_string(),
                    ButtonPressed::Up => "release_2();".to_string(),
                    ButtonPressed::Down => "release_3();".to_string(),
                },
                Function::Transition(target) => {
                    let state_var_name = "state";
                    let target_id = match state_ids.get(&StateName::Name(target.to_string())) {
                        Some(t) => t,
                        None => panic!("Unknown Target"),
                    };

                    format!("{} = {}; return;", state_var_name, target_id)
                }
                Function::TransitionWithPause(target, delay) => {
                    let state_var_name = "state";
                    let target_id = match state_ids.get(&StateName::NameWaiting(target.to_string()))
                    {
                        Some(t) => t,
                        None => panic!("Unknown Target"),
                    };

                    format!(
                        "wait_time = {}; clock_gettime(CLOCK_MONOTONIC_RAW, &start); {} = {}; return;",
                        delay.as_micros(), state_var_name, target_id
                    )
                }
            },
        }
    }
}

impl State {
    fn generate_state_ids(&self, ids: &mut HashMap<StateName, usize>, current: &mut usize) {
        ids.insert(StateName::Name(self.name.to_string()), *current);
        ids.insert(StateName::NameWaiting(self.name.to_string()), *current + 1);
        *current += 2;
    }

    fn generate(&self, state_ids: &HashMap<StateName, usize>) -> String {
        let mut body = String::new();
        for stmnt in self.body.iter() {
            body.push_str(&stmnt.generate(state_ids));
        }
        body
    }
}

impl StateMachine {
    pub fn generate(&self, template: TestTemplate, test_name: String, timeout: usize) -> String {
        let state_ids = {
            let mut tmp = HashMap::new();
            let mut current = 0;
            for state in self.states.iter() {
                state.generate_state_ids(&mut tmp, &mut current);
            }
            tmp
        };

        let run_switch = {
            let mut tmp = String::new();

            for state in self.states.iter() {
                let content = state.generate(&state_ids);

                let case_content = format!(
                    "// {}\ncase {}: {{ {} break; }} \n",
                    state.name,
                    state_ids
                        .get(&StateName::Name(state.name.to_string()))
                        .unwrap(),
                    content,
                );
                tmp.push_str(&case_content);
            }

            for state in self.states.iter() {
                let content = format!(
                    "
                    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
                    uint64_t delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;

                    if (delta_us > wait_time) {{
                        state = {};
                        return;
                    }}
                ", state_ids
                .get(&StateName::Name(state.name.to_string()))
                .unwrap(),
                );

                let case_content = format!(
                    "// {}-Waiting\ncase {}: {{ {} break; }} \n",
                    state.name,
                    state_ids
                        .get(&StateName::NameWaiting(state.name.to_string()))
                        .unwrap(),
                    content,
                );
                tmp.push_str(&case_content);
            }

            format!(
                "switch ({}) {{
                {}
                default: {{
                    break;
                }}
            }}",
                "state", tmp
            )
        };

        let run_func = format!("void run_sm() {{ {} }}", run_switch);

        let var_str = {
            let mut tmp = String::new();

            tmp.push_str(
                &(format!(
                    "int state = {};\n",
                    state_ids
                        .get(&StateName::Name("start".to_string()))
                        .unwrap()
                )),
            );
            tmp.push_str("struct timespec start, end;\n");
            tmp.push_str("uint64_t wait_time = 0;\n");

            tmp
        };

        let mut builder = template.builder();
        builder.test_name(test_name);
        builder.run_func(run_func);
        builder.run_call("run_sm();".to_string());
        builder.var_str(var_str);
        builder.timeout(timeout);

        builder.build()
    }
}
