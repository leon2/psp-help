#[derive(Debug, Clone)]
pub enum ButtonPressed {
    Enter,
    Esc,
    Up,
    Down,
}

impl ButtonPressed {
    pub fn parse(raw: &str) -> Option<Self> {
        match raw {
            "enter" => Some(Self::Enter),
            "esc" => Some(Self::Esc),
            "up" => Some(Self::Up),
            "down" => Some(Self::Down),
            _ => None,
        }
    }
}
