use crate::{
    span::{self, Span},
    tokenizer::Token,
};

pub struct ScopedIter<I, S>
where
    I: Iterator<Item = Span<S, Token>>,
{
    underlying: I,
    open: Token,
    close: Token,
    level: usize,
    done: bool,
}

impl<I, S> Iterator for ScopedIter<I, S>
where
    I: Iterator<Item = Span<S, Token>>,
    S: span::Source,
{
    type Item = Span<S, Token>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.done {
            return None;
        }

        let next = self.underlying.next()?;
        if next.content() == &self.open {
            self.level += 1;
        }
        if next.content() == &self.close {
            self.level -= 1;
            if self.level == 0 {
                self.done = self.level == 0;
                return None;
            }
        }

        Some(next)
    }
}

pub fn scoped_iter<I, S>(base: I, open: Token, close: Token) -> impl Iterator<Item = Span<S, Token>>
where
    I: Iterator<Item = Span<S, Token>>,
    S: span::Source,
{
    ScopedIter {
        underlying: base,
        open,
        close,
        level: 1,
        done: false,
    }
}
