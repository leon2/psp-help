pub struct TestTemplate {
    content: String,
}

pub struct TestTemplateBuilder<'t> {
    template: &'t TestTemplate,
    timeout: usize,
    test_name: String,
    var_str: String,
    run_func: String,
    run_call: String,
}

impl TestTemplate {
    pub fn new(content: String) -> Self {
        if !content.contains("#TIMEOUT") {
            panic!()
        }
        if !content.contains("#TESTNAME") {
            panic!()
        }
        if !content.contains("#STATEVARS") {
            panic!()
        }
        if !content.contains("#STATEMACHINE") {
            panic!()
        }
        if !content.contains("#RUNSM") {
            panic!()
        }

        Self { content }
    }

    pub fn builder(&self) -> TestTemplateBuilder<'_> {
        TestTemplateBuilder {
            template: self,
            timeout: 30,
            test_name: String::new(),
            var_str: String::new(),
            run_func: String::new(),
            run_call: String::new(),
        }
    }
}

impl<'t> TestTemplateBuilder<'t> {
    pub fn test_name(&mut self, test_name: String) {
        self.test_name = test_name;
    }
    pub fn var_str(&mut self, var_str: String) {
        self.var_str = var_str;
    }
    pub fn run_func(&mut self, run_func: String) {
        self.run_func = run_func;
    }
    pub fn run_call(&mut self, run_call: String) {
        self.run_call = run_call;
    }
    pub fn timeout(&mut self, timeout: usize) {
        self.timeout = timeout;
    }

    pub fn build(self) -> String {
        self.template
            .content
            .replace("#TIMEOUT", &format!("{}", self.timeout))
            .replace("#TESTNAME", &self.test_name)
            .replace("#STATEVARS", &self.var_str)
            .replace("#STATEMACHINE", &self.run_func)
            .replace("#RUNSM", &self.run_call)
    }
}
