pub struct CpuTemplate {
    content: String,
}

impl CpuTemplate {
    pub fn new<S>(content: S) -> CpuTemplate
    where
        S: Into<String>,
    {
        let content = content.into();

        if !content.contains("#CPU") {
            panic!("Missing #CPU")
        }

        Self { content }
    }

    pub fn generate(&self, cpu: &str) -> String {
        self.content.replace("#CPU", cpu)
    }
}
