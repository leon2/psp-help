use std::path::PathBuf;

pub struct MakefileTemplate {
    content: String,
}

pub struct MakefileTemplateBuilder<'t> {
    template: &'t MakefileTemplate,
    name: String,
    test_task_dir: PathBuf,
    spos_dir: PathBuf,
}

impl MakefileTemplate {
    pub fn new<S>(content: S) -> Self
    where
        S: Into<String>,
    {
        let content = content.into();

        if !content.contains("#TESTNAME") {
            panic!("Missing '#TESTNAME'")
        }
        if !content.contains("#TESTTASKDIR") {
            panic!("Missing '#TESTTASKDIR'")
        }
        if !content.contains("#SPOSDIR") {
            panic!("Missing '#SPOSDIR'")
        }

        Self { content }
    }

    pub fn builder(&self) -> MakefileTemplateBuilder<'_> {
        MakefileTemplateBuilder {
            template: self,
            name: String::new(),
            test_task_dir: PathBuf::new(),
            spos_dir: PathBuf::new(),
        }
    }
}

impl<'t> MakefileTemplateBuilder<'t> {
    pub fn name(mut self, name: String) -> Self {
        self.name = name;
        self
    }

    pub fn test_task_dir(mut self, path: PathBuf) -> Self {
        self.test_task_dir = path;
        self
    }

    pub fn spos_dir(mut self, path: PathBuf) -> Self {
        self.spos_dir = path;
        self
    }

    pub fn build(self) -> String {
        self.template
            .content
            .replace("#TESTNAME", &self.name)
            .replace("#TESTTASKDIR", self.test_task_dir.to_str().unwrap())
            .replace("#SPOSDIR", self.spos_dir.to_str().unwrap())
    }
}
