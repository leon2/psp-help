use std::{
    ffi::OsStr,
    fmt::{Debug, Display},
    path::PathBuf,
};

use ariadne::{Label, Report, ReportKind};
use clap::{Parser, Subcommand};
use psp_helper::{ParseError, ProcessError, SingleSourceCache};

/// A Program to handle all things related to Testing for the PSP module
#[derive(Parser, Debug)]
struct Args {
    #[command(subcommand)]
    action: Command,
}

#[derive(Debug, Subcommand)]
enum Command {
    /// Generate the correct C Test File for a single Test
    #[command(
        about = "Generate single Test Case from Statemachine Description",
        long_about
    )]
    GenTest {
        #[command(flatten)]
        config: SingleTestConfig,
    },
    /// Generates all the Tests that are specified in a given Directory. All files ending in .pst are considered State-Machine Configurations
    #[command(
        about = "Generate all the Test Cases specified in a Folder",
        long_about
    )]
    GenTests {
        #[command(flatten)]
        config: MultipleTestConfig,
    },
    /// Experimental:
    /// Generates all the Test Cases in a given Directory, like `gen-tests` and then also runs them all
    #[command(
        about = "[Experimental] Generate and run all the Test Cases specified in a Folder",
        long_about
    )]
    RunTests {
        #[command(flatten)]
        config: MultipleTestConfig,
    },
}

#[derive(Debug, Parser)]
struct SingleTestConfig {
    /// The Name of the Test
    #[arg(long)]
    test_name: String,

    /// The Path for the Input Statemachine File
    #[arg(long)]
    input: PathBuf,

    /// The Output Path for the resulting Test program
    #[arg(long, default_value = "test-build/test.c")]
    output: PathBuf,

    /// The Target CPU for the Tests
    #[arg(long, default_value = "atmega644")]
    cpu: String,

    /// The Timeout for the Test case in ms
    #[arg(long, default_value = "30")]
    timeout: usize,

    /// The Directory under which all the given TestTasks provided by the university are stored
    #[arg(long)]
    test_task_dir: PathBuf,

    /// The Root Directory of your source code
    #[arg(long)]
    src_dir: PathBuf,
}

#[derive(Debug, Parser)]
struct MultipleTestConfig {
    /// The Root Directory in which all the Tests are stored
    #[arg(long)]
    root_dir: PathBuf,

    /// The Output Directory in which all the Tests will be generated into
    #[arg(long, default_value = "test-build/")]
    output_dir: PathBuf,

    /// The Target CPU for the Tests
    #[arg(long, default_value = "atmega644")]
    cpu: String,

    /// The Timeout for the Test case in ms
    #[arg(long, default_value = "30")]
    timeout: usize,

    /// The Directory under which all the given TestTasks provided by the university are stored
    #[arg(long)]
    test_task_dir: PathBuf,

    /// The Root Directory of your source code
    #[arg(long)]
    src_dir: PathBuf,

    /// Used to filter out which tests are actually run, if only some should be run.
    /// Every test that contains the given String will be run
    #[arg(long, default_value = "")]
    test_name: String,
}

static RAW_TEST_TEMPLATE: &str = include_str!("../assets/template.tc");
static RAW_CPU_TEMPLATE: &str = include_str!("../assets/cpu_template.tc");
static RAW_MAKE_TEMPLATE: &str = include_str!("../assets/Makefile.template");

fn main() {
    let args = Args::parse();

    let helper_assets = psp_helper::asset::AssetFolder::new(
        "helper/",
        [
            Box::new(psp_helper::asset::AssetFile::new(
                "inputs.c",
                include_str!("../assets/helper/inputs.c"),
            )),
            Box::new(psp_helper::asset::AssetFile::new(
                "inputs.h",
                include_str!("../assets/helper/inputs.h"),
            )),
            Box::new(psp_helper::asset::AssetFile::new(
                "lcd.c",
                include_str!("../assets/helper/lcd.c"),
            )),
            Box::new(psp_helper::asset::AssetFile::new(
                "lcd.h",
                include_str!("../assets/helper/lcd.h"),
            )),
        ] as [Box<dyn psp_helper::asset::Asset>; 4],
    );

    match args.action {
        Command::GenTest { config, .. } => {
            let _ = generate_test(config);
        }
        Command::GenTests { config } => {
            let _ = generate_tests(config);
        }
        Command::RunTests { config } => {
            run_tests(config, &helper_assets);
        }
    };
}

fn display_error<S>(err: ProcessError<S>)
where
    S: psp_helper::Source,
{
    match err {
        ProcessError::Parse(perr) => match perr {
            ParseError::UnexpectedEOF => {
                todo!()
            }
            ParseError::UnexpectedToken { expected, received } => {
                let src = received.source();
                let src_cache = SingleSourceCache::new(src.clone());

                let expected_str = expected.display_str();

                Report::build(ReportKind::Error, src.id().to_owned(), 0)
                    .with_message("Unexpected Token")
                    .with_label(
                        Label::new(received).with_message(format!("Expected {}", expected_str)),
                    )
                    .finish()
                    .print(src_cache)
                    .unwrap();
            }
            ParseError::UnknownCondition(cond) => {
                let src = cond.source();
                let src_cache = SingleSourceCache::new(src.clone());

                Report::build(ReportKind::Error, src.id().to_owned(), 0)
                    .with_message("Unknown Condition")
                    .with_label(Label::new(cond).with_message("Unknown"))
                    .finish()
                    .print(src_cache)
                    .unwrap();
            }
            ParseError::UnknownFunction(func) => {
                let src = func.source();
                let src_cache = SingleSourceCache::new(src.clone());

                Report::build(ReportKind::Error, src.id().to_owned(), 0)
                    .with_message("Unknown Function")
                    .with_label(Label::new(func).with_message("Unknown"))
                    .finish()
                    .print(src_cache)
                    .unwrap();
            }
        },
    }
}

fn generate_tests(
    MultipleTestConfig {
        root_dir,
        output_dir,
        cpu,
        timeout,
        test_task_dir,
        src_dir,
        test_name,
    }: MultipleTestConfig,
) -> Result<Vec<PathBuf>, ()> {
    let mut result = Vec::new();
    let test_filter_name = test_name;

    let root_dir_list = std::fs::read_dir(root_dir).unwrap();
    for entry in root_dir_list.filter_map(|e| e.ok()) {
        let ty = match entry.file_type() {
            Ok(t) => t,
            Err(_) => continue,
        };
        if !ty.is_file() {
            continue;
        }

        let entry_path = entry.path();

        let raw_test_name = match entry_path.file_stem() {
            Some(n) => n,
            None => continue,
        };
        let test_name = match raw_test_name.to_str() {
            Some(n) => n,
            None => continue,
        };

        if !test_name.contains(&test_filter_name) {
            continue;
        }

        let output_path_dir = output_dir.join(format!("{}/", test_name));
        let output_path = output_path_dir.join("test.c");
        if !output_path_dir.exists() {
            std::fs::create_dir_all(&output_path_dir).expect("");
        }

        let config = SingleTestConfig {
            cpu: cpu.clone(),
            test_name: test_name.to_string(),
            input: entry_path.clone(),
            output: output_path,
            timeout,
            test_task_dir: test_task_dir.clone(),
            src_dir: src_dir.clone(),
        };

        generate_test(config)?;

        result.push(output_path_dir);
    }

    Ok(result)
}

fn generate_test(
    SingleTestConfig {
        test_name,
        input,
        output,
        cpu,
        timeout,
        test_task_dir,
        src_dir,
    }: SingleTestConfig,
) -> Result<(), ()> {
    let current_dir = std::env::current_dir().expect("");

    let source = psp_helper::FileSource::new(input).unwrap();
    let result = match psp_helper::process(source) {
        Ok(r) => r,
        Err(e) => {
            display_error(e);
            return Err(());
        }
    };

    let template = psp_helper::TestTemplate::new(RAW_TEST_TEMPLATE.to_string());
    let final_test = result.generate(template, test_name.to_string(), timeout);

    let prev_name = output.file_name().unwrap();
    let raw_name = {
        let mut tmp = OsStr::new(cpu.as_str()).to_os_string();

        tmp.push("_");
        tmp.push(prev_name);

        tmp
    };
    let cpu_output = output.with_file_name(raw_name);

    let cpu_template = psp_helper::CpuTemplate::new(RAW_CPU_TEMPLATE);
    let final_cpu = cpu_template.generate(cpu.as_str());

    let make_output = output.with_file_name("Makefile");
    let make_template = psp_helper::MakefileTemplate::new(RAW_MAKE_TEMPLATE);
    let make_final = make_template
        .builder()
        .name(test_name)
        .test_task_dir(std::fs::canonicalize(current_dir.join(test_task_dir)).unwrap())
        .spos_dir(std::fs::canonicalize(current_dir.join(src_dir)).unwrap())
        .build();

    let output_dir = output.parent().unwrap();
    if !output_dir.exists() {
        std::fs::create_dir_all(output_dir).unwrap();
    }
    std::fs::write(output, final_test).unwrap();
    std::fs::write(cpu_output, final_cpu).unwrap();
    std::fs::write(make_output, make_final).unwrap();

    Ok(())
}

fn run_tests<A>(config: MultipleTestConfig, helper_assets: &A)
where
    A: psp_helper::asset::Asset,
{
    let build_dir = config.output_dir.clone();

    if !config.output_dir.exists() {
        std::fs::create_dir_all(&config.output_dir).unwrap();
    }

    println!("===== Setting up Environment =====");

    let simavr_dir = build_dir.join("simavr/");
    let simavr_exmaples_dir = simavr_dir.join("examples/");
    if !simavr_dir.exists() {
        // git clone https://github.com/buserror/simavr.git;
        let clone_res = std::process::Command::new("git")
            .arg("clone")
            .arg("https://github.com/buserror/simavr.git")
            .current_dir(&build_dir)
            .output();

        let clone_out = match clone_res {
            Ok(o) => o,
            Err(e) => {
                panic!("Cloning simavr failed {:?}", e);
            }
        };
        if !clone_out.status.success() {
            let out_str = std::str::from_utf8(&clone_out.stdout).unwrap();
            let err_str = std::str::from_utf8(&clone_out.stderr).unwrap();
            panic!(
                "Clone failed \n\nStd-Out:\n{}\nStd-Err:\n{}",
                out_str, err_str
            );
        }

        let example_folder = simavr_exmaples_dir
            .read_dir()
            .expect("Could not read examples directory");

        for entry in example_folder.filter_map(|e| e.ok()) {
            let fty = entry.file_type().expect("Could not get Filetype");
            if !fty.is_dir() {
                continue;
            }

            let epath = entry.path();
            std::fs::remove_dir_all(epath).expect("Could not remove examples");
        }

        let install_simavr_res = std::process::Command::new("make")
            .arg("install-simavr")
            .current_dir(&simavr_dir)
            .output();

        let install_simavr_out = match install_simavr_res {
            Ok(o) => o,
            Err(e) => {
                panic!("Running make to install simavr {:?}", e);
            }
        };
        if !install_simavr_out.status.success() {
            let out_str = std::str::from_utf8(&install_simavr_out.stdout).unwrap();
            let err_str = std::str::from_utf8(&install_simavr_out.stderr).unwrap();
            panic!(
                "Make failed \n\nStd-Out:\n{}\nStd-Err:\n{}",
                out_str, err_str
            );
        }
    }

    // Modify lcd.c to exclude that weird delay part
    let lcd_path = config.src_dir.join("SPOS/lcd.c");
    let old_lcd_path = lcd_path.with_file_name("old_lcd.c.tmp");
    std::fs::copy(&lcd_path, &old_lcd_path).expect("Could not copy lcd.c to old_lcd.c");
    let prev_lcd_content =
        std::fs::read_to_string(&lcd_path).expect("We should be able to read lcd.c");
    let n_lcd_content =
        prev_lcd_content.replace("busy = LCD_PIN & 0x08;", "/* busy = LCD_PIN & 0x08; */");
    std::fs::write(&lcd_path, n_lcd_content).expect("We should be able to write to lcd.c");
    let restore_lcd = psp_helper::OnDrop::new(move || {
        std::fs::rename(&old_lcd_path, &lcd_path).expect("Could not restore lcd.c File");
        println!("Restored lcd.c File");
    });

    println!("===== Generating Tests =====");
    let tests = match generate_tests(config) {
        Ok(t) => t,
        Err(_) => return,
    };

    println!("\n===== Compiling Tests =====");
    // Setup the correct directory structure for the tests
    for test_dir in tests.iter() {
        let name = test_dir.file_stem().unwrap();
        println!("Setting up Test: {:?}", name);

        let example_test_dir = simavr_exmaples_dir.join(name);
        if !example_test_dir.exists() {
            std::fs::create_dir(&example_test_dir).unwrap();
        }

        for entry in std::fs::read_dir(test_dir).unwrap().filter_map(|e| e.ok()) {
            let fty = entry.file_type().unwrap();
            if !fty.is_file() {
                continue;
            }

            let epath = entry.path();

            let ename = epath.file_name().unwrap();
            let mut target_file = example_test_dir.join(ename);
            if let Some(ext) = epath.extension() {
                target_file = target_file.with_extension(ext);
            }

            if !target_file.exists() {
                std::fs::copy(entry.path(), target_file).unwrap();
            }
        }

        // Install the helper stuff
        helper_assets
            .write(&example_test_dir)
            .expect("Write Helper Assets into test folder");

        // make > build.log 2>&1;
        let make_res = std::process::Command::new("make")
            .current_dir(example_test_dir)
            .output();
        let make_out = match make_res {
            Ok(o) => o,
            Err(e) => {
                panic!("Error running Make: {:?}", e);
            }
        };
        if !make_out.status.success() {
            let out_str = std::str::from_utf8(&make_out.stdout).unwrap();
            let err_str = std::str::from_utf8(&make_out.stderr).unwrap();
            panic!(
                "Make failed \n\nStd-Out:\n{}\nStd-Err:\n{}",
                out_str, err_str
            );
        }
    }

    println!("\n===== Running Tests =====");
    let test_handles = tests
        .iter()
        .map(|test_dir| {
            let name = test_dir.file_stem().unwrap();
            println!("Running Test: {:?}...", name);

            let example_test_dir = simavr_exmaples_dir.join(name);

            let mut possible_obj_folders = std::fs::read_dir(example_test_dir)
                .unwrap()
                .filter_map(|entry| entry.ok())
                .filter_map(|entry| {
                    let fty = entry.file_type().ok()?;
                    if !fty.is_dir() {
                        return None;
                    }

                    Some(entry.path())
                })
                .filter(|e| match e.file_name() {
                    Some(n) => match n.to_str() {
                        Some(n) => n.starts_with("obj-"),
                        None => false,
                    },
                    None => false,
                });
            let obj_folder = possible_obj_folders.next().expect("Missing Object folder");

            let target_file = obj_folder.join("test.elf");
            let abs_target_file = std::fs::canonicalize(target_file).expect("");

            (
                name.to_os_string(),
                std::thread::spawn(|| {
                    std::process::Command::new(abs_target_file)
                        .current_dir(obj_folder)
                        .output()
                }),
            )
        })
        .collect::<Vec<_>>();

    let mut errors = Vec::new();
    for (name, test_result) in test_handles {
        let joined_res = test_result
            .join()
            .expect("Thread did not return successfully");

        let joined_out = match joined_res {
            Ok(o) => o,
            Err(e) => {
                errors.push((name, Box::new(e) as Box<dyn Display>));
                continue;
            }
        };

        if joined_out.status.success() {
            println!("Test {:?} Passed", name);
        } else {
            let std_out = std::str::from_utf8(&joined_out.stdout)
                .expect("The output should always be valid utf-8");
            let std_err = std::str::from_utf8(&joined_out.stderr)
                .expect("The output should always be valid utf-8");

            errors.push((
                name,
                Box::new(format!("Std-Out: \n{}\nStd-Err: \n{}", std_out, std_err)),
            ));
        }
    }

    for (name, error) in errors.iter() {
        println!("Test {:?} Failed: \n{}", name, error);
    }

    drop(restore_lcd);

    if !errors.is_empty() {
        panic!("Failed")
    }
}
