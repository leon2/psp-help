use std::{
    fmt::{Debug, Display},
    ops::Range,
    path::PathBuf,
    sync::Arc,
};

pub trait Source: Sized + Debug {
    type SrcId: PartialEq + ToOwned;

    fn id(&self) -> &Self::SrcId;
    fn display_id(&self) -> Box<dyn Display>;

    fn length(&self) -> usize;

    fn content(&self) -> &str;

    fn sub_span<C>(self, range: Range<usize>, content: C) -> Span<Self, C> {
        Span {
            source: self,
            area: range,
            content,
        }
    }
}

pub struct SingleSourceCache<S> {
    source: (S, ariadne::Source),
}

impl<S> SingleSourceCache<S>
where
    S: Source,
{
    pub fn new(source: S) -> Self {
        let asource = ariadne::Source::from(source.content());
        Self {
            source: (source, asource),
        }
    }
}
impl<S> ariadne::Cache<S::SrcId> for SingleSourceCache<S>
where
    S: Source,
{
    fn fetch(&mut self, id: &S::SrcId) -> Result<&ariadne::Source, Box<dyn std::fmt::Debug + '_>> {
        if self.source.0.id() != id {
            return Err(Box::new("Given ID is not a known source"));
        }

        Ok(&self.source.1)
    }

    fn display<'a>(&self, id: &'a S::SrcId) -> Option<Box<dyn std::fmt::Display + 'a>> {
        if self.source.0.id() != id {
            return None;
        }

        Some(self.source.0.display_id())
    }
}

#[derive(Debug, PartialEq)]
pub struct Span<S, C> {
    source: S,
    area: Range<usize>,
    content: C,
}

impl<S, C> Span<S, C>
where
    S: Source,
{
    pub fn new(source: S, content: C) -> Self {
        let len = source.length();
        Self {
            source,
            area: 0..len,
            content,
        }
    }

    pub fn source(&self) -> &S {
        &self.source
    }

    pub fn as_str(&self) -> &str {
        let raw_content = self.source.content();
        &raw_content[self.area.clone()]
    }

    pub fn content(&self) -> &C {
        &self.content
    }

    pub fn map<F, C2>(self, func: F) -> Span<S, C2>
    where
        F: FnOnce(C) -> C2,
    {
        Span {
            source: self.source,
            area: self.area,
            content: func(self.content),
        }
    }
}

impl<S, C> ariadne::Span for Span<S, C>
where
    S: Source,
{
    type SourceId = S::SrcId;

    fn source(&self) -> &Self::SourceId {
        self.source().id()
    }

    fn start(&self) -> usize {
        self.area.start
    }
    fn end(&self) -> usize {
        self.area.end
    }
}

impl<S> Source for Arc<S>
where
    S: Source,
{
    type SrcId = S::SrcId;

    fn id(&self) -> &Self::SrcId {
        let inner: &S = self.as_ref();
        inner.id()
    }
    fn display_id(&self) -> Box<dyn Display> {
        let inner: &S = self.as_ref();
        inner.display_id()
    }

    fn length(&self) -> usize {
        let inner: &S = self.as_ref();
        inner.length()
    }

    fn content(&self) -> &str {
        let inner: &S = self.as_ref();
        inner.content()
    }
}

pub struct FileSource {
    path: PathBuf,
    content: String,
}

impl FileSource {
    pub fn new(path: PathBuf) -> Result<Self, std::io::Error> {
        let content = std::fs::read_to_string(&path)?;

        Ok(Self { path, content })
    }
}

impl Debug for FileSource {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("FileSource")
            .field("path", &self.path)
            .finish()
    }
}
impl Source for FileSource {
    type SrcId = PathBuf;

    fn id(&self) -> &Self::SrcId {
        &self.path
    }
    fn display_id(&self) -> Box<dyn Display> {
        Box::new(format!("{:?}", self.path))
    }

    fn length(&self) -> usize {
        self.content.len()
    }

    fn content(&self) -> &str {
        &self.content
    }
}

pub struct StaticSource {
    name: &'static str,
    content: String,
}

impl StaticSource {
    pub fn new<S>(name: &'static str, content: S) -> Self
    where
        S: Into<String>,
    {
        Self {
            name,
            content: content.into(),
        }
    }
}

impl Debug for StaticSource {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("StaticSource")
            .field("name", &self.name)
            .finish()
    }
}
impl Source for StaticSource {
    type SrcId = &'static str;

    fn id(&self) -> &Self::SrcId {
        &self.name
    }
    fn display_id(&self) -> Box<dyn Display> {
        Box::new(self.name)
    }

    fn length(&self) -> usize {
        self.content.len()
    }

    fn content(&self) -> &str {
        &self.content
    }
}
